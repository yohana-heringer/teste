package main;
import junit.framework.TestCase;
import junit.framework.Assert;
import main.Resultado;

public class ResultadoTest extends TestCase
{
	private String[] onomatopoeiae = { "Fizz", "Buzz" };
	
	private int getRandomOnomatopoeia() {
		return (int) Math.floor(Math.random() * this.onomatopoeiae.length);
	}
	
	public void testDivisibleByThree()
	{
		String onomatopoeia = this.onomatopoeiae[getRandomOnomatopoeia()];
		Resultado resultado = new Resultado(3, onomatopoeia);
		Assert.assertEquals(onomatopoeia, resultado.generate(3));
		Assert.assertNotSame(onomatopoeia, resultado.generate(4));
	}
}
