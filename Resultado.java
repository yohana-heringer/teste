package main;

public class Resultado {
    private final int numero;
    private final String onomatopeia;
    private final Resultado proxNumero;

    public Resultado(int numero, String onomatopeia, Resultado proxNumero) {
        this.numero = numero;
        this.onomatopeia = onomatopeia;
        this.proxNumero = proxNumero;
    }

    public Resultado(int numero, String resultado) {
        this(numero, resultado, null);
    }

	public String generate(int i) {
        StringBuilder sb = new StringBuilder();
        generate(sb, i);
        return sb.length() == 0 ? String.valueOf(i) : sb.toString();
    }

    private void generate(StringBuilder sb, int i) {
        if (i % numero == 0)
            sb.append(onomatopeia);
        if (proxNumero != null) 
            proxNumero.generate(sb, i);
    }

}
