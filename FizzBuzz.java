package main;
import java.lang.Math;
import junit.framework.*;

public class FizzBuzz {
 
    public static void main(String[] args) {
        Resultado resultado = new Resultado(3, "Fizz", new Resultado(5, "Buzz"));
        for (int i = 1; i <= 100; i++) {
            System.out.println(resultado.generate(i));
        }
    }
 
}